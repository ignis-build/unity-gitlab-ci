using Ignis.Unity.Building.Batch;
using UnityEditor;

namespace Ignis.Unity.GitLab.CI.Jobs
{
    // ReSharper disable once UnusedType.Global
    public static class Export
    {
        // ReSharper disable once UnusedMember.Global
        public static void VSSolution()
        {
            BatchScope.Run(() =>
            {
                AssetDatabase.SaveAssets();

                var generator = new ProjectGeneration();
                generator.Sync();
            });
        }
    }
}