using Ignis.Unity.Test.Tools.Runners;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.GitLab.CI.Jobs
{
    // ReSharper disable once UnusedType.Global
    public static class Test
    {
        private static UnityTestRunner Runner { get; } = new();

        // ReSharper disable once UnusedMember.Global
        public static void EditMode()
        {
            Run(TestMode.EditMode);
        }

        // ReSharper disable once UnusedMember.Global
        public static void PlayMode()
        {
            Run(TestMode.PlayMode);
        }

        private static void Run(TestMode mode)
        {
            Runner.Execute(new ExecutionSettings(new Filter
                {
                    testMode = mode
                }))
                .WithCallback(context =>
                {
                    context.PrintSummary();
                    context.ToJUnitReport().Save();

                    context.ExitEditorIfBatchmode();
                });
        }
    }
}