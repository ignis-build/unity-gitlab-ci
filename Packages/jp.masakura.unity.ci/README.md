# Unity GitLab CI
GitLab CI job template for [Unity](https://unity.com/).

[unityci/editor](https://hub.docker.com/r/unityci/editor) is used.


## How to use
1. Add ci job template to your `.gitlab-ci.yml`.
2. Run request unity license file job.
3. Obtain a license manually.
4. Set unity license to `UNITY_ULF` project CI/CD variables.
5. Add unity build job.


### Add ci job to your `.gitlab-ci.yml`
Create a new `.gitlab-ci.yml` file to your unity project.

If CI/CD components are to be used, the following. (Recommended)

```yaml
include:
  - components: gitlab.com/ignis-build/unity-gitlab-ci/core@master
```

If you use the job template, do the following.

```yaml
include: https://gitlab.com/ignis-build/unity-gitlab-ci/-/raw/master/ci/templates/unity.yml
```

And push to GitLab your project.


### Run request unity license file job
When the push is finished, a CI job is run to create the unity license request file.

![](docs/images/request-alf-job.png)

And download `Unity_v202x.x.xx.alf`.

![](docs/images/download-alft.png)


### Obtain a license manually
Upload the `Unity_v202x.x.xx.alf` file at [unity's manual licensing site](https://license.unity3d.com/manual) to obtain the license file (`Unity_v202x.ulf`).

See [Manual license activation](https://docs.unity3d.com/Manual/ManualActivationGuide.html).


### Set unity license to `UNITY_ULF` project CI/CD variables
Go to your GitLab project CI/CD variables setting page.

Open the `Unity_v202x.ulf` file in a text editor and copy its contents to the clipboard.

Add `UNITY_ULF` CI/CD variables, and Pastes the contents of the clipboard. `Type` should be changed to `File`.

![](./docs/images/cicd-variables.png)


### Add unity build job
Create unity C# class for unity build. (Ex: `Assets/Editor/Build.cs`)

```csharp
using UnityEditor;
using UnityEditor.Build.Reporting;

public static class Build
{
    public static void WebGL()
    {
        var report = BuildPipeline.BuildPlayer(
            new[] { "Assets/Scenes/SampleScene.unity" },
            "dist/webgl",
            BuildTarget.WebGL,
            BuildOptions.None);

        if (report.summary.result != BuildResult.Succeeded)
        {
            EditorApplication.Exit(1);
        }
        EditorApplication.Exit(0);
    }
}
```

Add unity build job to your `.gitlab-ci.yml`.

```yaml
build:webgl:
  extends: .unity
  stage: build
  script:
    - $UNITY_CMD -executeMethod Build.WebGL
  artifacts:
    name: webgl
    paths: [dist]
```

Once committed and pushed, the Unity build CI job is started.

Once the CI job completes, you can download the Unity app for WebGL.

![](./docs/images/download-unity-app.png)


## Settings
Use CI/CD components.(See [templates/core/template.yml](templates/core/template.yml))

```yaml
include:
  - components: gitlab.com/ignis-build/unity-gitlab-ci@master
    inputs:
      unity-version: '2022.3.16f1'
      unity-editor: unity-editor
      unity-opts: -logfile -
      unity-ci-platform: webgl
      unity-ci-version: 3
```

Use job variables.

```yaml
job:
  variables:
    UNITY_OPTS: -logfile -
    UNITY_CMD: $UNITY_EDITOR $UNITY_OPTS
    # UNITY_VERSION: '2021.3.15f1' # Defaults to auto-detect from Unity project
    UNITY_CI_PLATFORM: webgl # `android` or `windows-mono` or etc... (see https://hub.docker.com/r/unityci/editor/tags)
    UNITY_CI_VERSION: 3 # 1 or 2 or 3
```


## Optional jobs
There are additional following jobs.

* test (Unity Test)

These jobs require the Unity GitLab CI package for execution.


### Install Unity GitLab CI package
Add npmjs registry to Packages/manifest.json in your Unity project.

```json
{
  "scopedRegistries": [
    {
      "name": "npmjs",
      "url": "https://registry.npmjs.com",
      "scopes": ["jp.masakura.unity"]
    }
  ],
"dependencies": {
// ...
```

1. Open Package Manager Window with Your Unity project.
2. Click +, select Add package by name...
3. entry jp.masakura.unity.ci
4. Click Add


### test
It runs Unity's EditMode and PlayMode tests. The results are output in JUnit report format and sent to GitLab. You can access the test results from Merge Requests and so on.

It adds CI/CD components to your `.gitlab-ci.yml` file.

```yaml
include:
  - components: gitlab.com/ignis-build/unity-gitlab-ci/core@master
  - components: gitlab.com/ignis-build/unity-gitlab-ci/test@master
```


### inspect code
Inspect the code with ReSharper's [InspectCode](https://www.jetbrains.com/help/resharper/InspectCode.html) and display the inspection results in [GitLab Code Quality](https://docs.gitlab.com/ee/ci/testing/code_quality.html).

It adds CI/CD components to your `.gitlab-ci.yml` file.

```yaml
include:
  - components: gitlab.com/ignis-build/unity-gitlab-ci/core@master
  - components: gitlab.com/ignis-build/unity-gitlab-ci/inspect-code@master
    inputs:
      ensure-severity: suggestion # If there is a severity higher than 'suggestion', the job will fail. 
```


## Attention
[GitLab SaaS Runner](https://docs.gitlab.com/ee/ci/runners/) does not perform very well. We recommend using a Runner that uses a project-specific docker executor.

## License
This repository is [MIT](LICENSE) licensed.
