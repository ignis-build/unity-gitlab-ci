using System;
using System.Reflection;
using Packages.Rider.Editor.UnitTesting;

namespace Ignis.Unity.GitLab.CI.Jobs
{
    internal sealed class ProjectGeneration
    {
        private readonly object _instance = Activator.CreateInstance(Type);
        private static Assembly Assembly { get; } = typeof(RiderTestRunner).Assembly;

        private static Type Type { get; } =
            Assembly.GetType("Packages.Rider.Editor.ProjectGeneration.ProjectGeneration", true);

        private static MethodInfo SyncMethod { get; } =
            Type.GetMethod(nameof(Sync), BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

        public void Sync()
        {
            SyncMethod.Invoke(_instance, Array.Empty<object>());
        }
    }
}