﻿using System;
using Ignis.Unity.Building.Batch;
using UnityEditor;
using UnityEditor.Build.Reporting;

// ReSharper disable once CheckNamespace
// ReSharper disable once UnusedType.Global
public static class Build
{
    // ReSharper disable once UnusedMember.Global
    public static void WebGL()
    {
        BuildPlayer("dist/webgl", BuildTarget.WebGL);
    }

    // ReSharper disable once UnusedMember.Global
    public static void Android()
    {
        BuildPlayer("dist/android/sample.apk", BuildTarget.Android);
    }

    private static void BuildPlayer(string locationPathName, BuildTarget buildTarget)
    {
        BatchScope.Run(() =>
        {
            var report = BuildPipeline.BuildPlayer(
                new[] { "Assets/Scenes/SampleScene.unity" },
                locationPathName,
                buildTarget,
                BuildOptions.None);

            if (report.summary.result != BuildResult.Succeeded)
                throw new InvalidOperationException("Build failed.");
        });
    }
}